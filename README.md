## Introduction

In this exercice, we need to start three differents containers in order to upload code from two of them onto the git server. My two docker clients are the famous Alice and Bob. 
To complete this task, we will need three `Dockerfile` and one `docker-compose.yml` for rule them all.

### Prerequisites : 

A GNU/Linux machine to run docker and a network connection 

> [!Important] Important
> You need to install these packages :
> - `docker`
> - `docker-compose`
> - `git`
> - `nano`

For a **Debian/Ubuntu** configuration :
```bash
sudo apt update && sudo apt install docker docker-compose git nano
```

For those who prefer BTW **Arch** 😀 :
```bash
sudo pacman -Syu docker docker-compose git nano tree
```

And finally for GNU/Linux distribution who are based on RHEL (like Fedora):
```bash
sudo dnf install docker docker-compose git nano
```


And finally, do a git clone of our project :
```bash
git clone https://forge.univ-lyon1.fr/p2312055/git-docker.git
```


So, if you enter in the project and check the files :
```
[kali@kali tpdocker]$ tree  
.  
├── Alice  
│   └── Dockerfile  
├── Bob  
│   └── Dockerfile  
├── docker-compose.yml  
├── git-server  
│   └── Dockerfile  
├── init_client.sh  
├── init_copytobob.sh  
└── initial_setup.sh  
  
4 directories, 7 files
```

# Dockerfile
### git-server

In our exercice, we need to have a git-server who will host the main branch of our git repository.
So, to do that, we need a server who can accept ssh connection and git to host the git branch.

> [!todo] Requirements
> Packets we need here :
> - `openssh-server`
> - `git`



In the `Dockerfile` of the **git-server** :

```bash
# Pull the latest Ubuntu Image
FROM ubuntu:latest

# Installation SSH server and Git
RUN apt update && apt install -y openssh-server git

# Create ssh folder
RUN mkdir -p /run/sshd && \
    chmod 755 /run/sshd

# Creation of the folder for the Volume share
RUN mkdir -p /root/.ssh_transfer

# Creation of bob and alice users
RUN useradd -m -s /bin/bash bob
RUN useradd -m -s /bin/bash alice

#Create User git and configure git repository
RUN useradd -m -p "$(openssl passwd -1 'gitpass')" git -s /bin/bash
USER git
RUN mkdir -p /home/git/repos/projet1
WORKDIR /home/git/repos/projet1
RUN git init --bare


# Create SSH key
RUN mkdir -p /home/git/.ssh
RUN touch /home/git/.ssh/authorized_keys

# Open 22 port
EXPOSE 22 

# Copy and run the bash script
USER root
COPY initial_setup.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/initial_setup.sh
ENTRYPOINT ["/usr/local/bin/initial_setup.sh"]
WORKDIR /root/
```


> [!info] Info
> The keys are located in ``/home/git/.ssh/``
> 

### docker-compose.yml

`docker-compose.yml` is a tool for defining and running multi-container applications, so we will use to orchestrate the creation of our 3 dockers.

In our docker-compose, we will create these instructions :

- Build the 3 dockers but they all need to wait the end of instruction of the Alice docker
- Create a Volume share at `./shared_keys:/root/.ssh_transfer`
- Create a Network with an network address **172.20.0.0/16**


Content of the `docker-compose.yml` :

```docker-compose
version: "3.3"


services:
  git-server:
    container_name: git-server
    build: 
      context: .
      dockerfile: git-server/Dockerfile
    image: git-serveur
    networks:
      mynet:
        ipv4_address: 172.20.0.5
    ports:
    - "2222:22"
    volumes:
      - ./shared_keys:/root/.ssh_transfer
    depends_on:
      - Alice


  Alice:
    container_name: Alice
    build: 
      context: .
      dockerfile: Alice/Dockerfile
    image: alice-ssh
    networks:
      mynet:
        ipv4_address: 172.20.0.2
    ports:
    - "2223:22"
    volumes:
      - ./shared_keys:/root/.ssh_transfer


  Bob:
    container_name: Bob
    build: 
      context: .
      dockerfile: Bob/Dockerfile
    image: bob-ssh
    networks:
      mynet:
        ipv4_address: 172.20.0.3
    ports:
    - "2224:22"
    volumes:
      - ./shared_keys:/root/.ssh_transfer
    depends_on:
      - Alice

networks:
  mynet:
    driver: bridge
    ipam:
     config:
     - subnet: 172.20.0.0/16

```

To execute  the `docker-compose.yml`:
```bash
[user@yourmachine tpdocker]$ docker-compose up
```


### Alice - Client

> [!info]
> In our configuration, Alice is the main client machine, her main jobs is
> - Create an SSH key in the git user SSH repository
> - Copy the ==Public Key== and the ==Private Key== in the Share Volume `/root/.ssh_transfert`
> - Create the git repository and synchronise it the **git-server** repository

Alice `Dockerfile` :

```bash
# Pull the last Ubuntu Image
FROM ubuntu:latest

# Installation SSH Client and Git
RUN apt update && apt install -y openssh-client git


# Create the git user
RUN useradd git -s /bin/bash -m -d /home/git

# Create .ssh_transfert folder
USER root
RUN mkdir -p /root/.ssh_transfer/


#Create .ssh/ folder and SSH key
COPY init_client.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/init_client.sh
ENTRYPOINT ["/usr/local/bin/init_client.sh"]

# Configure Git
USER git
WORKDIR /home/git/
RUN mkdir -p /home/git/repos/projet1
WORKDIR /home/git/repos/projet1
RUN git init
RUN git remote add origin ssh://git@172.20.0.5/home/git/repos/projet1

#Open SSH port
USER root
EXPOSE 22

WORKDIR /root/
```


### Bob - Client

> [!info]
> In our configuration, bob is the second client machine, his main jobs is
> - Get the ==Private key== of  Alice in `/root/.ssh_transfert` and ==move-it== in his git user `.ssh/` folder
> - Create the git repository and synchronise it the **git-server** repository

Bob `Dockerfile`:
```bash
# Pull the last Ubuntu Image
FROM ubuntu:latest

# Installation SSH Client and Git
RUN apt update && apt install -y openssh-client git


# Create the user git
RUN useradd git -s /bin/bash -m -d /home/git

# Configure git folder
USER git
WORKDIR /home/git/
RUN mkdir -p /home/git/repos/projet1
WORKDIR /home/git/repos/projet1
RUN git init
RUN git remote add origin ssh://git@172.20.0.5/home/git/repos/projet1

#Open SSH port
EXPOSE 22

#Create SSH folder
USER git
RUN mkdir -p /home/git/.ssh/
USER root
RUN mkdir -p /root/.ssh_transfer/

#Copy the SSH key
COPY init_copytobob.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/init_copytobob.sh
ENTRYPOINT ["/usr/local/bin/init_copytobob.sh"]

WORKDIR /root/
```

### Scripts

> [!info]
> For our docker-compose to work properly, we need 3 scripts :
> - `initial_setup.sh`
> - `init_copytobob.sh `
> - `init_client.sh`  


To remind you :
```bash

├── Alice  
│   └── Dockerfile  
├── Bob  
│   └── Dockerfile  
├── docker-compose.yml  
├── git-server  
│   └── Dockerfile  
├── init_client.sh
├── init_copytobob.sh
└── initial_setup.sh
```

The  script `init_client.sh` is executed by Alice `Dockerfile`. 
His goal is to create SSH RSA keys and to copy them on the Volume Share.

After, the script `init_copytobob` is executed by bob to get the SSH private to him `.ssh/` folder (logic). The Private SSH key is moved, not copied.

Finally, the script `initial_setup.sh` is executed by the server. His goal is to copy the content of the public key of their git client into his file `authorized_keys` who permit to allow git users.

### Tips

To build individually each image from `Dockerfile` :
```bash
cd git-server
sudo docker build -t git-server .
```



### Troubleshoot 
##### Rebuild Properly your images 

To rebuild  properly with `docker-compose.yml`, follow these steps :

```bash
docker rmi git-server:latest docker rmi alice-ssh:latest bob-ssh:latest
```

> [!error] Tips
> If you have an error, you can force it with :
> 
> `docker rmi [images] --force`

```bash
docker prune system
```

And finally execute the command:

```bash
docker-compose up --build
```


Le Tyrant Louis and Masure Nicolas
#!/bin/bash

# Create the ".ssh/" folder and set git to the owner of this folder 
mkdir -p /home/git/.ssh/
chown git:git /home/git/.ssh/

# Create the SSH key
ssh-keygen -t rsa -C "" -N "" -f /home/git/.ssh/id_rsa

# Set git of the owner of the Public and Private Key.
chown git:git /home/git/.ssh/id_rsa && \
chown git:git /home/git/.ssh/id_rsa.pub

# Copy Public Key in the share volume
cp /home/git/.ssh/id_rsa.pub  /root/.ssh_transfer
cp /home/git/.ssh/id_rsa  /root/.ssh_transfer

# Command to prevent the container from exiting immediately
tail -f /dev/null
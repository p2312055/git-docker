#!/bin/bash

# A busy way to wait the script "init_client.sh"
sleep 5

# Copy the Public Key in authorized_keys
cat /root/.ssh_transfer/id_rsa.pub >> /home/git/.ssh/authorized_keys

# Remove the Public Key in the share Volume and start SSH server
rm /root/.ssh_transfer/id_rsa.pub
service ssh start

# Command to prevent the container from exiting immediately
tail -f /dev/null


